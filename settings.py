USER_AGENTS = {
    'web': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) '
           'AppleWebKit/605.1.15 (KHTML, like Gecko)',
    'mobile': 'Mozilla/5.0 (Linux; U; Android 2.2) AppleWebKit/533.1 (KHTML, '
              'like Gecko) Version/4.0 Mobile Safari/533.1 '
}

GOOGLE_REQUEST_COUNT = 2
DEFAULT_LOCATION_CODE = 'tr'
DEFAULT_DEVICE = 'mobile'
