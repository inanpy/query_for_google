# Query for Google #

Verilen parametre ile google sorgusunda çıkan sitelere erişerek
 içerisideki external linkleri listeler.

### Requirements  ###

* Python 

### Deployment ###

````
git clone git@bitbucket.org:inanpy/query_for_google.git
cd query_for_google
virtualenv QFG
source QFG/bin/activate
pip install -r requirements.txt
python api.py 
````

### Endpoint ###
**Request:**
```
Post /query/
{
    "location": "tr",
    "query": "query params",
    "count": 2,
    "device": "mobile"
}
```

**Response:**
```
HTTP Status: 200 OK
{
    "https://m.youtube.com/channel/UCfIqCzQJXvYj9ssCoHq327g/videos": [
        "https://www.google.com.tr/intl/tr/policies/privacy/"
    ],
    "https://m.youtube.com/watch?v=e5btJ0_KTU8": [
        "https://www.google.com.tr/intl/tr/policies/privacy/"
    ]
}
```

### Request Params ###
| Params Key    | Description   | Required  | Default | Type |
|:-------------:|:-------------:|:-----:|:--------:|:---------:|
| `location`    | Request Location | `False`| "tr" | String |
| `count`       | Google Request Count  | `False`|  2 | Integer |
| `device`      | Request Device Type   |  `False`|  "mobile" | String |
| `query`      | Request Query   | `True` | | String|


### Settings Params ###
| Settings Key    | Description  | Type|
|:-------------:|:-------------:|:---------:|
|`USER_AGENTS`| Common format for user agent | Dict ` only key:('web', 'mobile') `|
|`GOOGLE_REQUEST_COUNT`|  Request count for Google | Integer|
|`DEFAULT_LOCATION_CODE`| Request from which country | String `Supported country code for Google`|
|`DEFAULT_DEVICE`| Device type for request | String `only 'web' or 'mobile' `|

## Supported Country Codes ###
https://developers.google.com/custom-search/docs/xml_results_appendices#countryCodes

