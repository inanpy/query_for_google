# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from flask import Flask, request, Response
from flask_restful import Resource, Api
from settings import USER_AGENTS, GOOGLE_REQUEST_COUNT,\
    DEFAULT_LOCATION_CODE, DEFAULT_DEVICE
from googlesearch import search
import json
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

app = Flask(__name__)
api = Api(app)


class QueryEndPoint(Resource):

    def __init__(self):
        self.headers = None
        self.request_count = GOOGLE_REQUEST_COUNT
        self.default_location_code = DEFAULT_LOCATION_CODE
        self.default_device = DEFAULT_DEVICE

    def post(self):
        """
        Gelen isteğin içerisindeki parametreleri alarak send_google_search_request
        göndererek dönen respontaki urlleri find_site_urls e gönderiyoruz.
        """
        data = json.loads(request.data)
        query = data.get('query')
        if not query:
            return Response('Query Not Found', status=400)
        location = data.get('location_code', self.default_location_code)
        device = data.get('device', self.default_device)
        count = data.get('count', self.request_count)
        search_urls = self.send_google_search_request(
            query, location, device, count)
        response = self.find_site_urls(search_urls)
        return response

    def find_site_urls(self, search_urls):
        """
        Gelen Urllere istek atarak içerisindeli htmlleri parse edip aranan
        urller ile response  hazırlıyor.

        :param search_urls: Search edilecek urller.
        """
        response = {}
        for base_url in search_urls:
            response[base_url] = []
            res = requests.get(base_url)
            page = str(BeautifulSoup(res.content))
            while True:
                url, n = self.get_site_urls(page)
                page = page[n:]
                if url and 'http' in url and not base_url.split('/')[2] in url:
                    response[base_url].append(url)
                elif not url:
                    break
                else:
                    continue
        return response

    @staticmethod
    def get_site_urls(page):
        """
        Gelen HTML içerisindeli a href'leri find ediyor,
        :param page: Siteye ait parse edilmiş html.
        """
        start_link = page.find("a href")
        if start_link == -1:
            return None, 0
        start_quote = page.find('"', start_link)
        end_quote = page.find('"', start_quote + 1)
        url = page[start_quote + 1: end_quote]
        return url, end_quote

    @staticmethod
    def get_user_agent(type):
        """
        Settings üzerindeki USER_AGENTS keyindeki dict'e bakıyor.
        :param type: sadece 'mobile' veya 'web' değeri alıyor.
        :return:
        """
        return USER_AGENTS.get(type, 'web')

    @staticmethod
    def normalize_query(query):
        """
        Gelen string değeri maplenmiş karakterler ile replace ediyor.
        :param query: String değer.
        """
        replace_chacters = {
            'ı': 'i',
            'ğ': 'g',
            'ü': 'u',
            'ş': 's',
            'ö': 'o',
            'ç': 'c',
            'Ğ': 'G',
            'Ü': 'U',
            'Ş': 'S',
            'İ': 'I',
            'Ö': 'O',
            'Ç': 'C',
            ' ': '+'
        }
        for k, v in replace_chacters.items():
            query = query.replace(k, v)
        return query

    def send_google_search_request(self, query, location, device, count):
        """
        Gelen parametrelere göre google'a istek atıyor ve urlleri dönüyor.
        :param query: Seach edilecek parametre.
        :param location: Google da lokasyon bazlı arama için kullanıyor.
        :param device: Sadece 'mobil' veya 'web' olarak değer alıyor.
        :param count: Google'dan kaç adet url istiyoruz.
        """
        query = self.normalize_query(query)
        urls = search(
            query, country=location,
            user_agent=self.get_user_agent(device), stop=count
        )
        return urls


api.add_resource(QueryEndPoint, '/query/', endpoint='query')

if __name__ == '__main__':
    app.run(debug=True)
